public class StringBuilderExample {
    public static void main(String args[]) {
        StringBuilder sb = new StringBuilder("Amit");
        String s = sb.toString();
        Boolean b = s.isEmpty();
        System.out.println(b);

        sb.append("sharma");
        sb.insert(4, "kumar");
        String s1 = sb.toString();
        System.out.println(s1);
    }
}
