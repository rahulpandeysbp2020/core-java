public class StringCompareTo
{
    public static void main(String args[]) {
        String a = "apple";
        String b = "ball";
        String c = "cat";
        String d = "dog";
        System.out.println(a.compareTo(b));
        System.out.println(b.compareTo(a));
    }

}
