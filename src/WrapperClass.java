public class WrapperClass {
    Integer value = 30;
    Integer value1 = new Integer(30);

    Long id = 30L;
    Long id1 = new Long(30L);

    String name = "Rahul";
    String name1 = new String("Rahul");

    String roll = "1234";
    Integer r = Integer.parseInt(roll);
    Integer r1 = Integer.valueOf(r);

    public Integer getRoll(){
        return r1;
    }

    public Integer getR(){
        return r;
    }

}
