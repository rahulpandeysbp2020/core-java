package collectiondemo.listdemo;

public class Employee {
    private Long id;
    private Integer age;
    private Double employeeSalary;
    private String name;

    public Employee(){}

    public Employee(Long id,Integer age,Double employeeSalary,String name){
        this.id=id;
        this.age=age;
        this.employeeSalary=employeeSalary;
        this.name=name;
    }

    public String toString(){
        return "Employee Name is "+name;
    }

    public Long getId(){
       return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Integer getAge(){
        return age;
    }

    public void setAge(Integer age){
        this.age=age;
    }

    public Double getEmployeeSalary(){
        return employeeSalary;
    }

    public void setEmployeeSalary(Double employeeSalary){
        this.employeeSalary=employeeSalary;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;

    }

}
