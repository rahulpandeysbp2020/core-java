package collectiondemo.listdemo;

import java.util.ArrayList;
import java.util.List;

public class EmployeeList {

    public static void main(String args[]){
        List<Employee> employeeList = new ArrayList<Employee>();
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setAge(23);
        employee.setEmployeeSalary(23000.0);
        employee.setName("Rahul");
        employeeList.add(0,employee);

        Employee employee1 = new Employee();
        employee1.setId(2L);
        employee1.setAge(25);
        employee1.setEmployeeSalary(26000.0);
        employee1.setName("Pooja");
        employeeList.add(1,employee1);

        for(Employee emp : employeeList){
            System.out.println(emp);
        }

    }
}
