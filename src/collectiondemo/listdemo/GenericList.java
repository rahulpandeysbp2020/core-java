package collectiondemo.listdemo;

import java.util.ArrayList;

public class GenericList {
    public static void main(String args[]) {
        ArrayList<Integer> numberList = new ArrayList<Integer>();
        ArrayList<String> nameList = new ArrayList<String>();
        Boolean a1 = numberList.add(123);
        numberList.add(456);
        numberList.add(789);
        System.out.println(a1);

        nameList.add("rahul");
        nameList.add("pooja");
        nameList.add("gudia");

        for (int i = 0; i < numberList.size(); i++) {
            System.out.println(numberList.get(i));
        }

        System.out.println("**********");

        for (String a : nameList) {
            System.out.println(a);
        }
    }
}
