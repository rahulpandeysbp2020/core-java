package collectiondemo.listdemo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListDemoOne {
    public static void main(String args[]) {
    List<String> fruitList = new ArrayList<String>();
    fruitList.add("Mango");
    fruitList.add("Orange");
    fruitList.add("Grapes");
    fruitList.add(1,"Banana");
    fruitList.set(2,"Pineapple");
    fruitList.remove("Orange");
    for(String a :fruitList){
        System.out.println(a);
    }

    if(fruitList.contains("Mango")){
        System.out.println("rahul eat some Mangoes");
    }

    if(!fruitList.isEmpty()){
        System.out.println("fruitlist is not empty");

    }
    List<Integer> numberList = new LinkedList<Integer>();
    numberList.add(123);
    numberList.add(456);
    numberList.add(2,789);

    System.out.println(" New List");
        for (Integer a:numberList
             ) {
            System.out.println(a);
        }
    }
}

