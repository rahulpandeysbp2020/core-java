package collectiondemo.listdemo;

public class Student {
    private Long id;
    private Integer age;
    private String name;
    private Integer standard;

    public Student(Long id, Integer age, String name, Integer standard) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.standard = standard;
    }

    public Student(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStandard() {
        return standard;
    }

    public void setStandard(Integer standard) {
        this.standard = standard;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                ", standard=" + standard +
                '}';
    }
}

