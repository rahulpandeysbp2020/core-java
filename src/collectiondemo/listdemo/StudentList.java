package collectiondemo.listdemo;

import java.util.ArrayList;
import java.util.List;

public class StudentList {
    public static void main(String args[]) {
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(new Student(1L,12,"rahul",6));
        studentList.add(new Student(2L,22,"pooja",12));
        System.out.println(studentList);
    }
}
