package collectiondemo.mapdemo;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {
    public static void main(String args[]){
        Map<Integer,String> anyMap = new HashMap<Integer,String>();
        anyMap.put(1,"rahul");
        anyMap.put(2,"pooja");
        anyMap.put(3,"gudia");
        for(Map.Entry<Integer,String> a : anyMap.entrySet()){
            System.out.println("Key is " +a.getKey() + " Value is "+a.getValue());
        }
    }
}
