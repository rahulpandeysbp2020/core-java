package collectiondemo.mapdemo;

import java.util.HashMap;
import java.util.Map;

public class MapDemo2
{
public static void main(String args[]){
    Map<Integer,String> nameMap = new HashMap<Integer, String>();
    nameMap.put(123,"mukul");
    nameMap.put(456,"Buchi");
    nameMap.put(789,"Daksh");
    nameMap.remove(456);
    for (Map.Entry<Integer,String > a: nameMap.entrySet()){
        System.out.println("Key is "+a.getKey() + " Value is "+ a.getValue());
    }
}
}
