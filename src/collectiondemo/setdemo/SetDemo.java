package collectiondemo.setdemo;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {
    public static void main(String args[]){
        Set<String> nameSet = new HashSet<String>();
        nameSet.add("rahul");
        nameSet.add("pooja");
        nameSet.add("gudia");

        for (String a:nameSet){
            System.out.println(a);

        }
    }
}
